﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommonBehaviour : MonoBehaviour 
{
	public float shrinkSpeed, jumpDuration;
	private float jumpTime;
	private ObjectProperties properties;
	public ObjectProperties Properties{get{return properties;}}
	private IElementBehaviour elementBehaviour;
	private List <CommonBehaviour> absorbees, shrinkers;
	private LevelGeneration level;
	GlobalVariables global;

	void Awake()
	{
		properties = GetComponent<ObjectProperties>();
		level = Camera.main.GetComponent<LevelGeneration>();
		GameObject globaObj = GameObject.Find("Global Variables") as GameObject;
		global = globaObj.GetComponent<GlobalVariables>();
		setElement(properties.currentElement);
		absorbees = new List<CommonBehaviour>();
		shrinkers = new List<CommonBehaviour>();
	}

	void Update()
	{
		for(int i=0; i<absorbees.Count; i++)
		{
			if(absorbees[i] == null)
				absorbees.Remove(absorbees[i--]);
			else
			{
				absorbees[i].shrink(getScaleFactor(this, absorbees[i]));
				grow (getScaleFactor(absorbees[i], this));
			}
		}
		for(int i=0; i<shrinkers.Count; i++)
		{
			if(shrinkers[i] == null)
				shrinkers.Remove(shrinkers[i--]);
			else
			{
				shrink(getScaleFactor(shrinkers[i], this));
			}
		}
		if(transform.localScale.x <= level.LevelScale/100 || !global.gameBounds.Contains(transform.position))
		{
			die();
		}

		//Temp fix for OnTriggerExit not being called when scaling objects. Also see element baheviours' Trigger functions. 
		shrinkers.Clear();
		absorbees.Clear();

		if(properties.isPlayer && ((int)transform.localScale.x)%5 == 0)
			level.scaleUp(transform.localScale.x);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(elementBehaviour != null)
		{
			elementBehaviour.enterTrigger(other.GetComponent<CommonBehaviour>());
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(elementBehaviour != null)
		{
			elementBehaviour.exitTrigger(other.GetComponent<CommonBehaviour>());
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if(elementBehaviour != null)
		{
			elementBehaviour.stayTrigger(other.GetComponent<CommonBehaviour>());
		}
	}

	public void startShrinking(CommonBehaviour other)
	{
		if(!shrinkers.Contains(other))
			shrinkers.Add(other);
	}

	public void stopShrinking(CommonBehaviour other)
	{
		shrinkers.Remove(other);
	}

	public void grow(float scale)
	{
		Vector3 change = Vector2.one * (scale)*100 * Time.deltaTime;
		transform.localScale += change;
	}

	public void shrink(float scale)
	{
	//	float area = Mathf.PI * transform.localScale.x/2 * transform.localScale.x/2;
		Vector3 change = Vector2.one * (scale)*100 * Time.deltaTime;
		transform.localScale -= change;
	}

	public void die()
	{
		if(properties.isPlayer)
		{
			InputController.resetInputNumbers();
			Application.LoadLevel(Application.loadedLevel);
		}
		else
			Destroy(gameObject);
	}

	public void setElement(string element)
	{
		switch(element)
		{
		case "Earth":
			elementBehaviour = new EarthBehaviour(this);
			break;
		case "Wind":
			elementBehaviour = new WindBehaviour(this);
			break;
		case "Fire":
			elementBehaviour = new FireBehaviour(this);
			break;
		case "Water":
			elementBehaviour = new WaterBehaviour(this);
			break;
		}
	}

	public void stopAbsorbing(CommonBehaviour other)
	{
		absorbees.Remove(other);
	}

	public void absorb(CommonBehaviour other)
	{
		if(transform.localScale.x > other.transform.localScale.x)
		{
			if(!absorbees.Contains(other))
				absorbees.Add(other);
		}
		else if(transform.localScale.x == other.transform.localScale.x)
		{
			if(properties.isPlayer && !other.properties.isPlayer)
			{
				if(!absorbees.Contains(other))
					absorbees.Add(other);
			}
			else
			{
				shrink (getScaleFactor(this, other));
			}
		}
	}

	private float getScaleFactor(CommonBehaviour one, CommonBehaviour other)
	{
		float scale = 0;
		float radius1 = one.transform.localScale.x/2;
		float radius2 = other.transform.localScale.x/2;
		float distance = (other.transform.position - one.transform.position).magnitude;

		if (distance > radius2 + radius1)
		{
			return scale = 0;
		}
		
		// Circle1 is completely inside circle0
		else if (distance <= Mathf.Abs(radius1 - radius2) && radius1 >= radius2)
		{
			// Return area of circle1
			scale = Mathf.PI * radius2*radius2;
		}
		
		// Circle0 is completely inside circle1
		else if (distance <= Mathf.Abs(radius1 - radius2) && radius1 < radius2)
		{
			// Return area of circle0
			scale = Mathf.PI * radius1*radius1;
		}

		// Circles partially overlap
		else
		{
			float phi = (Mathf.Acos((radius1*radius1 + (distance * distance) - radius2*radius2) / (2 * radius1 * distance))) * 2;
           	var theta = (Mathf.Acos((radius2*radius2 + (distance * distance) - radius1*radius1) / (2 * radius2 * distance))) * 2;
            float area1 = 0.5f * theta * radius2*radius2 - 0.5f * radius2*radius2 * Mathf.Sin(theta);
            float area2 = 0.5f * phi * radius1*radius1 - 0.5f * radius1*radius1 * Mathf.Sin(phi);
			
			// Return area of intersection
			scale = area1 + area2;
		}

		scale /= Mathf.PI * radius1*radius1;
		scale *= one.transform.localScale.x / other.transform.localScale.x;

		return scale;

		/*if(distance == 0 || Mathf.Abs(distance) < radius1+radius2)
			return ((radius1 <= radius2) ? Mathf.PI*radius1*radius1 : Mathf.PI*radius2*radius2);  */
		/*if(radius2 < radius1){
			// swap
			radius1 = radius2;
			radius2 = radius1;
		}

		float part1 = radius1*radius1*Mathf.Acos((distance*distance + radius1*radius1 - radius2*radius2)/(2*distance*radius1));
		float part2 = radius2*radius2*Mathf.Acos((distance*distance + radius2*radius2 - radius1*radius1)/(2*distance*radius2));
		float part3 = 0.5f*Mathf.Sqrt((-distance+radius1+radius2)*(distance+radius1-radius2)*(distance-radius1+radius2)*(distance+radius1+radius2));
		
		return part1 + part2 - part3;*/
	}
}
