﻿using UnityEngine;
using System.Collections;

public class GlobalVariables : MonoBehaviour {

	public int numPlayers;
	public bool[] usingKeyboard;
	public GameObject elementPrefab;
	public Rect cameraRect;
	public Rect gameBounds;

	// Use this for initialization
	void Start () 
	{
		DontDestroyOnLoad(gameObject);
	}

	public void calculateCameraBounds()
	{
		float cameraHeight = Camera.main.orthographicSize*2;
		float cameraWidth = cameraHeight*((float)Screen.width/Screen.height);
		cameraRect = new Rect(Camera.main.transform.position.x - cameraWidth/2, 
		                             Camera.main.transform.position.y - cameraHeight/2, 
		                             cameraWidth, cameraHeight);
		
		gameBounds = new Rect(cameraRect);
		gameBounds.xMax += gameBounds.width/5;
		gameBounds.xMin -= gameBounds.width/5;
		gameBounds.yMin -= gameBounds.height/5;
		gameBounds.yMax += gameBounds.height/5;
	}
}
