﻿using UnityEngine;
using System.Collections;

public class LevelGeneration : MonoBehaviour
{
	public GameObject elementPrefab;
	private float cameraWidth, cameraHeight;
	private float previousTime;
	private float levelScale;
	public float LevelScale{get{return levelScale;}}
	private GlobalVariables global;
	private bool scaling;
	private float lerpT;

	void Start()
	{
		GameObject globalObj = GameObject.Find ("Global Variables") as GameObject;
		global = globalObj.GetComponent<GlobalVariables>();
		levelScale = 10;
		previousTime = Time.time;
		Random.seed = (int)Time.time;
	}

	void Update()
	{
		if(Time.time - previousTime >= 1)
		{
			generateEnemy();
			previousTime = Time.time;
		}

		if(scaling)
		{
			if(lerpT < 1)
			{
				camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, levelScale, lerpT);
				lerpT += 0.1f*Time.deltaTime;
				global.calculateCameraBounds();
			}
			else
				scaling = false;
		}
	}

	private void generateEnemy()
	{
		Vector2 pos = randomPosAlongBorder();
		Vector2 velocity = randomVelocity(pos);

		GameObject newEnemy = Instantiate(elementPrefab, pos, Quaternion.identity) as GameObject;
		newEnemy.rigidbody2D.velocity = velocity;
		float scale = Random.Range(0.001f*levelScale, 0.25f*levelScale);
		newEnemy.transform.localScale = new Vector2(scale, scale);
		string element = "";
		switch(Random.Range(1, 5))
		{
		case 1:
			element = "Fire";
			break;
		case 2:
			element = "Wind";
			break;
		case 3:
			element = "Water";
			break;
		case 4:
			element = "Earth";
			break;
		}
		newEnemy.GetComponent<CommonBehaviour>().setElement(element);
	}

	private Vector2 randomVelocity(Vector2 pos)
	{
		Vector2 vel = global.cameraRect.center - pos;
		vel.Normalize();

		vel *= Random.Range(1, 5);
		return vel;
	}

	private Vector2 randomPosAlongBorder()
	{
		Vector2 pos = new Vector2((Random.Range (global.gameBounds.xMin, global.gameBounds.xMax)), (Random.Range (global.gameBounds.yMin, global.gameBounds.yMax)));
		if(global.cameraRect.Contains(pos))
			return randomPosAlongBorder();
		else
			return pos;
	}

	public void scaleUp(float scale)
	{
		if(levelScale <= scale)
		{
			lerpT = 0;
			levelScale += 20;
			scaling = true;
		}
	}
}
