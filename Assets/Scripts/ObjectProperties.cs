using UnityEngine;
using System.Collections;

public class ObjectProperties : MonoBehaviour 
{
	public Vector2 velocity;
	public float acceleration, slowingAcceleration;
	public bool accelerating, isPlayer;
	public float maxSpeed;
	public string currentElement;
	public Sprite fire, water, earth, wind;

	void Update()
	{
		//Slow down
		if(!accelerating && velocity.magnitude != 0)
		{
			velocity -= (velocity.normalized * slowingAcceleration) * Time.deltaTime;
		}

		//Limit velocity
		if(velocity.magnitude > maxSpeed)
		{
			velocity = velocity.normalized * maxSpeed;
		}

		if(isPlayer)
		{
			bindToCamera();
			//bindCamera();
		}

		gameObject.transform.Translate(velocity * Time.deltaTime);
	}

	private void bindToCamera()
	{
		Rect cameraRect;
		cameraRect = new Rect();
		Vector3 cameraPos = Camera.main.transform.position;
		float halfHeight = Camera.main.orthographicSize;
		float halfWidth = halfHeight * ((float)Screen.width/Screen.height);
		cameraRect.yMin = cameraPos.y - halfHeight;
		cameraRect.yMax = cameraPos.y + halfHeight;
		cameraRect.xMin = cameraPos.x - halfWidth;
		cameraRect.xMax = cameraPos.x + halfWidth;
		
		if(transform.position.x > cameraRect.xMax)
		{
			if(velocity.x > 0)
				velocity.x = 0;
		}
		else if(transform.position.x < cameraRect.xMin)
		{
			if(velocity.x < 0)
				velocity.x = 0;
		}
		if(transform.position.y > cameraRect.yMax)
		{
			if(velocity.y > 0)
				velocity.y = 0;
		}
		else if(transform.position.y < cameraRect.yMin)
		{
			if(velocity.y < 0)
				velocity.y = 0;
		}

	}

	private void bindCamera()
	{
		if(transform.position.x > Camera.main.transform.position.x)
		{
			Vector3 newCameraPos = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
			newCameraPos.x = transform.position.x;
			Camera.main.transform.position = newCameraPos;
		}
	}
}
