﻿using UnityEngine;
using System.Collections;

public class PreGameMenu : MonoBehaviour 
{
	public float menuWidth, menuHeight;
	private bool numPlayersChosen;
	private GlobalVariables global;

	// Use this for initialization
	void Start () 
	{
		GameObject globalObj = GameObject.Find("Global Variables") as GameObject;
		global = globalObj.GetComponent<GlobalVariables>();
	}

	void OnGUI()
	{
		if(!numPlayersChosen)
			chooseNumberOfPlayers();
		else
			selectPlayerInput();

		if(GUI.Button(new Rect(0, Screen.height - Screen.height/6, Screen.width/10, Screen.height/6), "BACK"))
		{
			if(numPlayersChosen)
				numPlayersChosen = false;
			else
				Application.LoadLevel(0);
		}

		if(numPlayersChosen)
			if(GUI.Button(new Rect(Screen.width - Screen.width/10, Screen.height - Screen.height/6, Screen.width/10, Screen.height/6), "START"))
			{
				Application.LoadLevel(4);
			}
	}

	private void chooseNumberOfPlayers()
	{
		GUI.Label(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 - menuHeight/2*5, menuWidth, menuHeight), "HOW MANY PLAYERS");

		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 - menuHeight/2*2, menuWidth, menuHeight), "1"))
		{
			global.numPlayers = 1;
			global.usingKeyboard = new bool[1];
			numPlayersChosen = true;
		}
		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 - menuHeight/2*0, menuWidth, menuHeight), "2"))
		{
			global.numPlayers = 2;
			global.usingKeyboard = new bool[2];
			numPlayersChosen = true;
		}
		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 + menuHeight/2*2, menuWidth, menuHeight), "3"))
		{
			global.numPlayers = 3;
			global.usingKeyboard = new bool[3];
			numPlayersChosen = true;
		}
		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 + menuHeight/2*4, menuWidth, menuHeight), "4"))
		{
			global.numPlayers = 4;
			global.usingKeyboard = new bool[4];
			numPlayersChosen = true;
		}
	}

	private void selectPlayerInput()
	{
		Rect outputRect = new Rect(0, 0, Screen.width/6, Screen.width/6);

		switch(global.numPlayers)
		{
		case 1:
			displayOnePlayerInputMenu(outputRect);
			break;
		case 2:
			displayTwoPlayerInputMenu(outputRect);
			break;
		case 3:
			displayThreePlayerInputMenu(outputRect);
			break;
		case 4:
			displayFourPlayerInputMenu(outputRect);
			break;
		}
	}

	private void displayOnePlayerInputMenu(Rect rect)
	{
		Rect one = new Rect(rect);
		one.center = new Vector2(Screen.width/2, Screen.height/2);

		displayPlayerInputMenu(one, "PLAYER 1", 1);
	}
	
	private void displayTwoPlayerInputMenu(Rect rect)
	{
		Rect one = new Rect(rect);
		Rect two = new Rect(rect);
		one.center = new Vector2(Screen.width/3, Screen.height/2);
		two.center = new Vector2(Screen.width/3*2, Screen.height/2);
		displayPlayerInputMenu(one, "PLAYER 1", 1);
		displayPlayerInputMenu(two, "PLAYER 2", 2);
	}
	
	private void displayThreePlayerInputMenu(Rect rect)
	{
		Rect one = new Rect(rect);
		Rect two = new Rect(rect);
		Rect three = new Rect(rect);
		one.center = new Vector2(Screen.width/2, Screen.height/4);
		two.center = new Vector2(Screen.width/5*2, Screen.height/4*3);
		three.center = new Vector2(Screen.width/5*3, Screen.height/4*3);
		displayPlayerInputMenu(one, "PLAYER 1", 1);
		displayPlayerInputMenu(two, "PLAYER 2", 2);
		displayPlayerInputMenu(three, "PLAYER 3", 3);
	}
	
	private void displayFourPlayerInputMenu(Rect rect)
	{
		Rect one = new Rect(rect);
		Rect two = new Rect(rect);
		Rect three = new Rect(rect);
		Rect four = new Rect(rect);
		one.center = new Vector2(Screen.width/4, Screen.height/4);
		two.center = new Vector2(Screen.width/4*3, Screen.height/4);
		three.center = new Vector2(Screen.width/4, Screen.height/4*3);
		four.center = new Vector2(Screen.width/4*3, Screen.height/4*3);
		displayPlayerInputMenu(one, "PLAYER 1", 1);
		displayPlayerInputMenu(two, "PLAYER 2", 2);
		displayPlayerInputMenu(three, "PLAYER 3", 3);
		displayPlayerInputMenu(four, "PLAYER 4", 4);
	}

	private void displayPlayerInputMenu(Rect rect, string label, int player)
	{
		
		GUI.Label(new Rect(rect.xMin, rect.yMin - rect.height/4, rect.width, rect.height /3), label);

		global.usingKeyboard[player-1] = !GUI.Toggle(new Rect(rect.xMin, rect.yMin, rect.width, rect.height/2), !global.usingKeyboard[player-1], "CONTROLLER");

		global.usingKeyboard[player-1] = GUI.Toggle(new Rect(rect.xMin, rect.yMax - rect.height/2, rect.width, rect.height/2), global.usingKeyboard[player-1], "KEYBOARD");
	}
}
