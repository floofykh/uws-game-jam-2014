﻿using UnityEngine;
using System.Collections;

public class HowToPlayMenu : MonoBehaviour {
	
	public float menuWidth, menuHeight;
	public Texture2D howToPlayImage;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), howToPlayImage);

		if(GUI.Button(new Rect(Screen.width - menuWidth, Screen.height - menuHeight, menuWidth, menuHeight), "BACK"))
		{
			Application.LoadLevel(0);
		}
	}
}
