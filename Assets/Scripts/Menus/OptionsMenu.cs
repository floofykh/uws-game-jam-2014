﻿using UnityEngine;
using System.Collections;

public class OptionsMenu : MonoBehaviour {
	
	public float menuWidth, menuHeight;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 - menuHeight/2, menuWidth, menuHeight), "BACK"))
		{
			Application.LoadLevel(0);
		}
	}
}
