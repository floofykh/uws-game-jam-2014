﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour 
{
	public float menuWidth, menuHeight;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 - menuHeight/2*3, menuWidth, menuHeight), "PLAY"))
		{
			Application.LoadLevel(3);
		}
		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 - menuHeight/2*1, menuWidth, menuHeight), "HOW TO PLAY"))
		{
			Application.LoadLevel(1);
		}
		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 + menuHeight/2*1, menuWidth, menuHeight), "OPTIONS"))
		{
			Application.LoadLevel(2);
		}
		if(GUI.Button(new Rect(Screen.width/2 - menuWidth/2, Screen.height/2 + menuHeight/2*3, menuWidth, menuHeight), "QUIT"))
		{
			Application.Quit();
		}
	}
}
