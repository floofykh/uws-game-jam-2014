﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
	private GlobalVariables global;

	// Use this for initialization
	void Start () 
	{
		GameObject globalObj = GameObject.Find("Global Variables") as GameObject;
		global = globalObj.GetComponent<GlobalVariables>();
		global.calculateCameraBounds();

		switch(global.numPlayers)
		{
		case 1:
			initOnePlayer();
			break;
		case 2:
			initTwoPlayer();
			break;
		case 3:
			initThreePlayer();
			break;
		case 4:
			initFourPlayer();
			break;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	private void initOnePlayer()
	{
		Vector2 pos1 = global.cameraRect.center;
		GameObject player1 = Instantiate(global.elementPrefab, pos1, Quaternion.identity) as GameObject;
		player1.GetComponent<CommonBehaviour>().setElement("Fire");
		player1.GetComponent<InputController>().setAsPlayer(1, global.usingKeyboard[0]);
	}
	private void initTwoPlayer()
	{
		Vector2 pos1 = new Vector2(global.cameraRect.center.x - global.cameraRect.width/5, global.cameraRect.center.y);
		Vector2 pos2 = new Vector2(global.cameraRect.center.x + global.cameraRect.width/5, global.cameraRect.center.y);
		GameObject player1 = Instantiate(global.elementPrefab, pos1, Quaternion.identity) as GameObject;
		GameObject player2 = Instantiate(global.elementPrefab, pos2, Quaternion.identity) as GameObject;
		player1.GetComponent<CommonBehaviour>().setElement("Fire");
		player2.GetComponent<CommonBehaviour>().setElement("Water");
		player1.GetComponent<InputController>().setAsPlayer(1, global.usingKeyboard[0]);
		player2.GetComponent<InputController>().setAsPlayer(2, global.usingKeyboard[1]);
	}
	private void initThreePlayer()
	{
		Vector2 pos1 = new Vector2(global.cameraRect.center.x - global.cameraRect.width/5, global.cameraRect.center.y);
		Vector2 pos2 = new Vector2(global.cameraRect.center.x + global.cameraRect.width/5, global.cameraRect.center.y);
		Vector2 pos3 = global.cameraRect.center;
		GameObject player1 = Instantiate(global.elementPrefab, pos1, Quaternion.identity) as GameObject;
		GameObject player2 = Instantiate(global.elementPrefab, pos2, Quaternion.identity) as GameObject;
		GameObject player3 = Instantiate(global.elementPrefab, pos3, Quaternion.identity) as GameObject;
		player1.GetComponent<CommonBehaviour>().setElement("Fire");
		player2.GetComponent<CommonBehaviour>().setElement("Water");
		player3.GetComponent<CommonBehaviour>().setElement("Earth");
		player1.GetComponent<InputController>().setAsPlayer(1, global.usingKeyboard[0]);
		player2.GetComponent<InputController>().setAsPlayer(2, global.usingKeyboard[1]);
		player3.GetComponent<InputController>().setAsPlayer(3, global.usingKeyboard[2]);
	}
	private void initFourPlayer()
	{
		Vector2 pos1 = new Vector2(global.cameraRect.center.x - global.cameraRect.width/5, global.cameraRect.center.y);
		Vector2 pos2 = new Vector2(global.cameraRect.center.x + global.cameraRect.width/5, global.cameraRect.center.y);
		Vector2 pos3 = new Vector2(global.cameraRect.center.x, global.cameraRect.center.y - global.cameraRect.height/5);
		Vector2 pos4 = new Vector2(global.cameraRect.center.x, global.cameraRect.center.y + global.cameraRect.height/5);
		GameObject player1 = Instantiate(global.elementPrefab, pos1, Quaternion.identity) as GameObject;
		GameObject player2 = Instantiate(global.elementPrefab, pos2, Quaternion.identity) as GameObject;
		GameObject player3 = Instantiate(global.elementPrefab, pos3, Quaternion.identity) as GameObject;
		GameObject player4 = Instantiate(global.elementPrefab, pos4, Quaternion.identity) as GameObject;
		player1.GetComponent<CommonBehaviour>().setElement("Fire");
		player2.GetComponent<CommonBehaviour>().setElement("Water");
		player3.GetComponent<CommonBehaviour>().setElement("Earth");
		player4.GetComponent<CommonBehaviour>().setElement("Earth");
		player1.GetComponent<InputController>().setAsPlayer(1, global.usingKeyboard[0]);
		player2.GetComponent<InputController>().setAsPlayer(2, global.usingKeyboard[1]);
		player3.GetComponent<InputController>().setAsPlayer(3, global.usingKeyboard[2]);
		player4.GetComponent<InputController>().setAsPlayer(4, global.usingKeyboard[3]);
	}
}
