﻿using UnityEngine;
using System.Collections;

public class NonPlayerInput : IInput 
{
	public bool escape()
	{
		return false;
	}

	public float vertical()
	{
		return 0;
	}
	public float horizontal()
	{
		return 0;
	}
	public bool up()
	{
		return false;
	}
	public bool down()
	{
		return false;
	}
	public bool left()
	{
		return false;
	}
	public bool right()
	{
		return false;
	}
	
	public bool leftReleased()
	{
		return false;
	}
	
	public bool rightReleased()
	{
		return false;
	}
	
	public bool upReleased()
	{
		return false;
	}
	
	public bool downReleased()
	{
		return false;
	}
	
	public bool switchToFire()
	{
		return false;
	}

	public bool switchToEarth()
	{
		return false;
	}

	public bool switchToWater()
	{
		return false;
	}

	public bool switchToWind()
	{
		return false;
	}

}
