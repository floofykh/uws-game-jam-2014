﻿using UnityEngine;
using System.Collections;

public class PlayerInput : IInput 
{
	public bool escape()
	{
		return false;
	}

	public float vertical()
	{
		return 0;
	}
	public float horizontal()
	{
		return 0;
	}
	public bool up()
	{
		return(Input.GetKey(KeyCode.UpArrow));
	}
	public bool down()
	{
		return(Input.GetKey(KeyCode.DownArrow));
	}
	public bool left()
	{
		return(Input.GetKey(KeyCode.LeftArrow));
	}
	public bool right()
	{
		return(Input.GetKey(KeyCode.RightArrow));
	}

	public bool leftReleased()
	{
		return(Input.GetKeyUp(KeyCode.LeftArrow));
	}
	
	public bool rightReleased()
	{
		return(Input.GetKeyUp(KeyCode.RightArrow));
	}
	
	public bool upReleased()
	{
		return(Input.GetKeyUp(KeyCode.UpArrow));
	}
	
	public bool downReleased()
	{
		return(Input.GetKeyUp(KeyCode.DownArrow));
	}

	public bool switchElement()
	{
		return(Input.GetKeyUp(KeyCode.Space));
	}
	
	public bool switchToFire()
	{
		return (Input.GetKeyUp(KeyCode.A));
	}
	
	public bool switchToEarth()
	{
		return (Input.GetKeyUp(KeyCode.S));
	}
	
	public bool switchToWater()
	{
		return (Input.GetKeyUp(KeyCode.D));
	}
	
	public bool switchToWind()
	{
		return (Input.GetKeyUp(KeyCode.F));
	}
}
