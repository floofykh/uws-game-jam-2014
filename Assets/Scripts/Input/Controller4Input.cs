using UnityEngine;
using System.Collections;

public class Controller4Input : IInput
{
	public bool escape()
	{
		return (Input.GetKeyUp(KeyCode.Escape));
	}

	public float vertical()
	{
		return(Input.GetAxis("Player3 - Controller - Vertical"));
	}
	
	public float horizontal()
	{
		
		return(Input.GetAxis("Player3 - Controller - Horizontal"));
	}
	public bool up()
	{
		return(Input.GetKey(KeyCode.UpArrow));
	}
	public bool down()
	{
		return(Input.GetKey(KeyCode.DownArrow));
	}
	public bool left()
	{
		return(Input.GetKey(KeyCode.LeftArrow));
	}
	public bool right()
	{
		return(Input.GetKey(KeyCode.RightArrow));
	}
	
	public bool leftReleased()
	{
		return(Input.GetKeyUp(KeyCode.LeftArrow));
	}
	
	public bool rightReleased()
	{
		return(Input.GetKeyUp(KeyCode.RightArrow));
	}
	
	public bool upReleased()
	{
		return(Input.GetKeyUp(KeyCode.UpArrow));
	}
	
	public bool downReleased()
	{
		return(Input.GetKeyUp(KeyCode.DownArrow));
	}
	
	public bool switchElement()
	{
		return(Input.GetKeyUp(KeyCode.Space));
	}
	
	public bool switchToFire()
	{
		return (Input.GetKeyUp(KeyCode.A));
	}
	
	public bool switchToEarth()
	{
		return (Input.GetKeyUp(KeyCode.S));
	}
	
	public bool switchToWater()
	{
		return (Input.GetKeyUp(KeyCode.D));
	}
	
	public bool switchToWind()
	{
		return (Input.GetKeyUp(KeyCode.F));
	}
}

