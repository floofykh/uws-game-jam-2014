using System;
		
public interface IInput
{
	bool escape();
	bool left();
	bool right();
	bool up();
	bool down();
	bool leftReleased();
	bool rightReleased();
	bool upReleased();
	bool downReleased();
	float vertical();
	float horizontal();
	bool switchToFire();
	bool switchToEarth();
	bool switchToWater();
	bool switchToWind();
}

