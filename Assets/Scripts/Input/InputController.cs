using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour 
{
	private IInput input;
	private static int numKeyboards, numControllers;
	private ObjectProperties properties;

	// Use this for initialization
	void Awake () 
	{
		properties = GetComponent<ObjectProperties>();

		if(properties.isPlayer)
			input = new PlayerInput();
		else
			input = new NonPlayerInput();
	}

	public void setAsPlayer(int num, bool keyboard)
	{
		properties.isPlayer = true;
		if(keyboard)
		{
			switch(numKeyboards)
			{
			case 0:
				input = new Keyboard1Input();
				numKeyboards++;
				break;
			case 1:
				input = new Keyboard2Input();
				numKeyboards++;
				break;
			case 2:
				input = new Keyboard3Input();
				numKeyboards++;
				break;
			case 3:
				input = new Keyboard4Input();
				numKeyboards++;
				break;
			}
		}
		else
		{
			switch(numControllers)
			{
			case 0:
				input = new Controller1Input();
				numControllers++;
				break;
			case 1:
				input = new Controller2Input();
				numControllers++;
				break;
			case 2:
				input = new Controller3Input();
				numControllers++;
				break;
			case 3:
				input = new Controller4Input();
				numControllers++;
				break;
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(properties.isPlayer)
		{
			/*if(input.up())
			{
				properties.velocity.y += properties.acceleration*Time.deltaTime;
				properties.accelerating = true;
			}
			else if(input.upReleased())
			{
				properties.accelerating = false;
			}

			if(input.down())
			{
				properties.velocity.y -= properties.acceleration*Time.deltaTime;
				properties.accelerating = true;
			}
			else if(input.downReleased())
			{
				properties.accelerating = false;
			}

			if(input.right ())
			{
				properties.velocity.x += properties.acceleration*Time.deltaTime;
				properties.accelerating = true;
			}
			else if(input.rightReleased())
			{
				properties.accelerating = false;
			}

			if(input.left ())
			{
				properties.velocity.x -= properties.acceleration*Time.deltaTime;
				properties.accelerating = true;
			}
			else if(input.leftReleased())
			{
				properties.accelerating = false;
			}

			if(input.switchToEarth())
			{
				behaviour.setElement("Earth");
			}
			if(input.switchToFire())
			{
				behaviour.setElement("Fire");
			}
			if(input.switchToWater())
			{
				behaviour.setElement("Water");
			}
			if(input.switchToWind())
			{
				behaviour.setElement("Wind");
			}*/

			float vertical = input.vertical();
			properties.velocity.x += properties.acceleration * input.horizontal();
			properties.velocity.y += properties.acceleration * input.vertical();

			if(input.horizontal() > 0 || input.vertical() > 0)
				properties.accelerating = true;
			else
				properties.accelerating = false;

			if(input.escape())
				Application.LoadLevel(0);
		}
	}

	public static void resetInputNumbers()
	{
		numControllers = 0;
		numKeyboards = 0;
	}
}
