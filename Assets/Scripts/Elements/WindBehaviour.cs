﻿using UnityEngine;
using System.Collections;

public class WindBehaviour : IElementBehaviour {

	public CommonBehaviour behaviour;
	private ObjectProperties properties;
	
	public WindBehaviour (CommonBehaviour behaviour) 
	{
		this.behaviour = behaviour;
		this.properties = behaviour.Properties;
		properties.currentElement = "Wind";
		behaviour.gameObject.GetComponent<SpriteRenderer>().sprite = properties.wind;
	}

	public void stayTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		/*case "Earth":
			
			break;*/
		case "Wind":
			behaviour.absorb(other);
			break;
		/*case "Fire":
			behaviour.jump();
			break;
		case "Water":
			
			break;*/
		}
	}
	public void exitTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		/*case "Earth":
			
			break;*/
		case "Wind":
			behaviour.stopAbsorbing(other);
			break;
		/*case "Fire":
			
			break;
		case "Water":
			break;*/
		}
	}
	
	public void enterTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		case "Earth":
			break;
		case "Wind":
			
			break;
		case "Fire":
			
			break;
		case "Water":
			break;
		}
	}
}
