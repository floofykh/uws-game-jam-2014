﻿using UnityEngine;
using System.Collections;

public class WaterBehaviour : IElementBehaviour
{
	public CommonBehaviour behaviour;
	private ObjectProperties properties;
	
	public WaterBehaviour (CommonBehaviour behaviour) 
	{
		this.behaviour = behaviour;
		this.properties = behaviour.Properties;
		properties.currentElement = "Water";
		behaviour.gameObject.GetComponent<SpriteRenderer>().sprite = properties.water;
	}

	public void stayTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
	/*	case "Earth":
			other.shrink();
			break;*/
		case "Wind":
			behaviour.startShrinking(other);
			other.startShrinking(behaviour);
			break;
		case "Fire":
			behaviour.startShrinking(other);
			other.startShrinking(behaviour);
			break;
		case "Water":
			behaviour.absorb(other);
			break;
		}
	}
	public void exitTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		/*case "Earth":
			behaviour.stopShrinking();
			other.stopShrinking();
			break;*/
		case "Wind":
			behaviour.stopShrinking(other);
			other.stopShrinking(behaviour);
			break;
		case "Fire":
			behaviour.stopShrinking(other);
			other.stopShrinking(behaviour);
			break;
		case "Water":
			behaviour.stopAbsorbing(other);
			break;
		}
	}
	
	public void enterTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		case "Earth":
			break;
		case "Wind":
			
			break;
		case "Fire":
			break;
		case "Water":
			break;
		}
	}
}
