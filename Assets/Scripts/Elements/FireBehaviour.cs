﻿using UnityEngine;
using System.Collections;

public class FireBehaviour : IElementBehaviour {

	public CommonBehaviour behaviour;
	private ObjectProperties properties;
	
	public FireBehaviour (CommonBehaviour behaviour) 
	{
		this.behaviour = behaviour;
		this.properties = behaviour.Properties;
		properties.currentElement = "Fire";
		behaviour.gameObject.GetComponent<SpriteRenderer>().sprite = properties.fire;
	}

	public void stayTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		/*case "Earth":
			
			break;*/
		case "Wind":
			behaviour.startShrinking(other);
			other.startShrinking(behaviour);
			break;
		case "Fire":
			behaviour.absorb(other);
			break;
		/*case "Water":
			behaviour.shrink();
			other.shrink();
			break;*/
		}
	}
	public void exitTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		/*case "Earth":
			
			break;*/
		case "Wind":
			behaviour.stopShrinking(other);
			other.stopShrinking(behaviour);
			break;
		case "Fire":
			behaviour.stopAbsorbing(other);
			break;
		/*case "Water":
			behaviour.stopShrinking();
			other.stopShrinking();
			break;*/
		}
	}
	
	public void enterTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		case "Earth":
			break;
		case "Wind":
			
			break;
		case "Fire":
			
			break;
		case "Water":
			break;
		}
	}
}
