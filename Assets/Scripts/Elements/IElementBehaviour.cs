
using System;

public interface IElementBehaviour
{
	void enterTrigger(CommonBehaviour other);
	void exitTrigger(CommonBehaviour other);
	void stayTrigger(CommonBehaviour other);
}

