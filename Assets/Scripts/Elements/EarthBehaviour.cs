﻿using UnityEngine;
using System.Collections;

public class EarthBehaviour : IElementBehaviour
{
	public CommonBehaviour behaviour;
	private ObjectProperties properties;

	public EarthBehaviour (CommonBehaviour behaviour) 
	{
		this.behaviour = behaviour;
		this.properties = behaviour.Properties;
		properties.currentElement = "Earth";
		behaviour.gameObject.GetComponent<SpriteRenderer>().sprite = properties.earth;
	}

	public void stayTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		case "Earth":
			behaviour.absorb(other);
			break;
		case "Wind":
			behaviour.startShrinking(other);
			other.startShrinking(behaviour);
			break;
		case "Fire":
			behaviour.startShrinking(other);
			other.startShrinking(behaviour);
			break;
		case "Water":
			behaviour.startShrinking(other);
			other.startShrinking(behaviour);
			break;
		}
	}

	public void exitTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		case "Earth":
			behaviour.stopAbsorbing(other);
			break;
		case "Wind":
			behaviour.stopShrinking(other);
			other.stopShrinking(behaviour);
			break;
		case "Fire":
			behaviour.stopShrinking(other);
			other.stopShrinking(behaviour);
			break;
		case "Water":
			behaviour.stopShrinking(other);
			other.stopShrinking(behaviour);
			break;
		}
	}
	
	public void enterTrigger(CommonBehaviour other)
	{
		switch(other.Properties.currentElement)
		{
		case "Earth":
			break;
		case "Wind":
			
			break;
		case "Fire":
			
			break;
		case "Water":
			break;
		}
	}
}
